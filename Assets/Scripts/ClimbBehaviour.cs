﻿using UnityEngine;
using System.Collections;

public class ClimbBehaviour : MonoBehaviour, GroundListener 
{
    public float minWallDistance;

    private LayerMask walkableMask = 1 << 8;

    public bool midAir;

    public bool wallNearby;
    public bool leftTouchingWall;
    public bool rightTouchingWall;
    public bool climbing;

    public float climbAcc;
    private float climbAccMax;
    public float climbAccDecayFactor;

    public float climbAccDecayFactor_specialCurrent;
    public float climbAccDecayFactor_specialMax;

    public Vector2 wallJumpForceInitial;
    
    public float minJumpDistForClimb;

    public Vector3 colliderOffsetBottom;

    public GameObject lastWall;

    private Sprite previousSpriteHalf;
    private Sprite previousSpriteFull;

    public Sprite climbSpriteHalf;
    public Sprite climbSpriteFull;

	// Use this for initialization
	void Start () 
    {
        Player.Instance.floater.groundListener.Add(this);
        climbAccMax = climbAcc;
        climbAccDecayFactor_specialCurrent = climbAccDecayFactor;

        colliderOffsetBottom = new Vector3(0, - this.collider2D.bounds.extents.y);
	}

    void FixedUpdate()
    {
        Ray2D rayLeft = new Ray2D(this.transform.position + colliderOffsetBottom, this.transform.right * -1);
        Ray2D rayRight = new Ray2D(this.transform.position + colliderOffsetBottom, this.transform.right);

        RaycastHit2D hitLeft = Physics2D.Raycast(rayLeft.origin, rayLeft.direction, 5, walkableMask);
        RaycastHit2D hitRight = Physics2D.Raycast(rayRight.origin, rayRight.direction, 5, walkableMask);

        leftTouchingWall = false;

        if (hitLeft.collider != null && (hitLeft.collider.gameObject.tag.Equals(Tags.wall) || hitLeft.collider.gameObject.tag.Equals(Tags.wall_special)))
        {
            float distance = this.transform.position.x - hitLeft.point.x;

            Debug.DrawLine(rayLeft.origin, hitLeft.point, Color.red);

            if (distance <= minWallDistance)
            {
                leftTouchingWall = true;
                
                this.transform.position = new Vector3(hitLeft.point.x + minWallDistance, this.transform.position.y, this.transform.position.z);
            }
        }

        rightTouchingWall = false;

        if (hitRight.collider != null && (hitRight.collider.gameObject.tag.Equals(Tags.wall) || hitRight.collider.gameObject.tag.Equals(Tags.wall_special)))
        {
            float distance = hitRight.point.x - this.transform.position.x;

            Debug.DrawLine(rayRight.origin, hitRight.point, Color.red);

            if (distance <= minWallDistance)
            {
                rightTouchingWall = true;
                
                this.transform.position = new Vector3(hitRight.point.x - minWallDistance, this.transform.position.y, this.transform.position.z);
            }
        }

        // has the wallNearby value changed?
        bool desiredValue = leftTouchingWall || rightTouchingWall;

        if (wallNearby != desiredValue)
        {
            wallNearby = desiredValue;

            if (wallNearby)
            {
                SetClimbSprites();
            }
            else
            {
                ResetSprites();
                climbing = false;
            }
        }

        // if wall nearby start the climbing-check
        if (wallNearby)
        {
            GameObject currentWall;

            if (leftTouchingWall)
            {
                if (hitLeft.transform.parent != null)
                {
                    currentWall = hitLeft.transform.parent.gameObject;
                }
                else
                {
                    currentWall = hitLeft.collider.gameObject;
                }
            }
            else
            {
                if (hitRight.transform.parent != null)
                {
                    currentWall = hitRight.transform.parent.gameObject;
                }
                else
                {
                    currentWall = hitRight.collider.gameObject;
                }
            }

            bool isSpecialWall = currentWall.tag.Equals(Tags.wall_special);

            // is it allowed to climb
            if (!climbing && midAir && Player.Instance.jumper.coveredDistance >= minJumpDistForClimb)
            {
                if (lastWall != null)
                {
                    if (currentWall.GetInstanceID() != lastWall.GetInstanceID())
                    {
                        lastWall = currentWall;
                        climbing = true;
                    }
                }
                else
                {
                    lastWall = currentWall;
                    climbing = true;
                }
            }

            // do the climbing 
            if (climbing)
            {
                if (isSpecialWall)
                {
                    climbAcc *= climbAccDecayFactor_specialCurrent;
                }
                else
                {
                    climbAcc *= climbAccDecayFactor;
                }

                if (climbAcc > 1)
                {
                    // actual climbing
                    if ((leftTouchingWall || rightTouchingWall))
                    {
                        if (Input.GetButton(Buttons.horizontal) && Player.Instance.controlsEnabled)
                        {
                            this.rigidbody2D.AddForce(new Vector2(0, this.rigidbody2D.mass * climbAcc));
                        }
                    }
                }
                else
                {
                    climbAcc = climbAccMax;
                    climbing = false;
                }

                // jump off wall
                if (Input.GetButtonDown(Buttons.jump) && Player.Instance.controlsEnabled)
                {
                    float factor = 1;

                    if (rightTouchingWall)
                        factor *= -1;

                    this.rigidbody2D.velocity = Vector3.zero;

                    this.rigidbody2D.AddForce(this.rigidbody2D.mass * new Vector2(wallJumpForceInitial.x * factor, wallJumpForceInitial.y), ForceMode2D.Impulse);

                    climbAcc = climbAccMax;
                    climbing = false;
                }
            }
        }
    }

	// Update is called once per frame
	void Update () 
    {
	
	}

    private void SetClimbSprites()
    {
        Player.Instance.attachedSaw.currentSpeed = Player.Instance.attachedSaw.fastSpeed;

        previousSpriteFull = Player.Instance.srFull.sprite;
        previousSpriteHalf = Player.Instance.srHalf.sprite;

        Player.Instance.srFull.sprite = climbSpriteFull;
        Player.Instance.srHalf.sprite = climbSpriteHalf;
    }

    private void ResetSprites()
    {
        Player.Instance.attachedSaw.currentSpeed = Player.Instance.attachedSaw.normalSpeed;

        Player.Instance.srFull.sprite = previousSpriteFull;
        Player.Instance.srHalf.sprite = previousSpriteHalf;
    }

    public void playerGroundEnter()
    {
        ResetSprites();

        lastWall = null;
        midAir = false;
        climbAcc = climbAccMax;
        climbing = false;
    }

    public void playerGroundExit()
    {
        midAir = true;
    }
}
