﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MuteButton : MonoBehaviour 
{
    public bool muted;

    public List<AudioSource> allAudioSources;

    public Sprite on;
    public Sprite off;

	// Use this for initialization
	void Start () 
    {
        this.transform.position = Camera.main.ViewportToWorldPoint(new Vector3(0.1f, 0.1f));
        this.transform.position += new Vector3(0, 0, 3);

        allAudioSources.AddRange(GameObject.FindObjectsOfType<AudioSource>());

        foreach (AudioSource current in allAudioSources)
        {
            current.enabled = !muted;
        }

        if (muted)
        {
            this.GetComponent<SpriteRenderer>().sprite = off;
        }
        else
        {
            this.GetComponent<SpriteRenderer>().sprite = on;
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
	}

    void OnMouseDown()
    {
        foreach (AudioSource current in allAudioSources)
        {
            current.enabled = muted;
        }

        muted = !muted;

        if (muted)
        {
            this.GetComponent<SpriteRenderer>().sprite = off;
        }
        else
        {
            this.GetComponent<SpriteRenderer>().sprite = on;
        }
    }
}
