﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FloatBehaviour : MonoBehaviour 
{
    public float minFloatHeight;
    public float gravityMultiplier = 1;

    public float maximumHeightCheck;

    private LayerMask walkableMask = 1 << 8;
    public bool applyGravity;

    public Collider2D coll;

    public Vector3 colliderOffsetLeft;
    public Vector3 colliderOffsetRight;

    public bool leftOverGround;
    public bool rightOverGround;

    public float leftHeight;
    public float rightHeight;

    public List<GroundListener> groundListener;

    public Vector3 lastGroundPos;
    public bool imWithPlayer;

    void Awake()
    {
        if (coll == null)
        {
            coll = this.collider2D;
        }

        groundListener = new List<GroundListener>();
    }

	// Use this for initialization
	void Start () 
    {
        applyGravity = false;

        colliderOffsetLeft = new Vector3(- coll.bounds.extents.x, 0);
        colliderOffsetRight = new Vector3( coll.bounds.extents.x, 0);

        if (Player.Instance.floater == this)
        {
            imWithPlayer = true;
        }
	}

    void FixedUpdate()
    {
        Ray2D rayLeft = new Ray2D(this.transform.position + colliderOffsetLeft, this.transform.up * -1);
        Ray2D rayRight = new Ray2D(this.transform.position + colliderOffsetRight, this.transform.up * -1);

        RaycastHit2D hitLeft = Physics2D.Raycast(rayLeft.origin, rayLeft.direction, maximumHeightCheck, walkableMask);
        RaycastHit2D hitRight = Physics2D.Raycast(rayRight.origin, rayRight.direction, maximumHeightCheck, walkableMask);
        //Debug.DrawRay(ray.origin, ray.direction * minPlayerHeight, Color.yellow);

        if (hitLeft.collider != null)
        {
            leftHeight = this.transform.position.y - hitLeft.point.y;

            Debug.DrawLine(rayLeft.origin, hitLeft.point, Color.red);

            if (leftHeight < minFloatHeight)
            {
                leftOverGround = true;
            }
            else if (leftHeight > minFloatHeight + 0.05f)
            {
                leftOverGround = false;
            }
        }
        else
        {
            leftHeight = float.MaxValue;
            leftOverGround = false;
        }

        if (hitRight.collider != null)
        {
            rightHeight = this.transform.position.y - hitRight.point.y;

            Debug.DrawLine(rayRight.origin, hitRight.point, Color.red);

            if (rightHeight < minFloatHeight)
            {
                rightOverGround = true;
            }
            else if (rightHeight > minFloatHeight + 0.05f)
            {
                rightOverGround = false;
            }
        }
        else
        {
            rightHeight = float.MaxValue;
            rightOverGround = false;
        }

        bool desiredValue = !(leftOverGround || rightOverGround);

        if (applyGravity != desiredValue)
        {
            applyGravity = desiredValue;

            if (applyGravity)
            {
                CallListenersExit();
            }
            else
            {
                if (imWithPlayer && Player.Instance.gonnaDie)
                {
                    this.transform.position = lastGroundPos + new Vector3(0, 2, 0);
                    Player.Instance.Respawn();
                    return;
                }

                CallListenersEnter();
            }
        }

        // apply Gravity when in air or float above the ground
        if (applyGravity)
        {
            this.rigidbody2D.AddForce(Physics2D.gravity * this.rigidbody2D.mass * gravityMultiplier);
        }
        else
        {
            float higherHitPoint = Mathf.Max(hitLeft.point.y, hitRight.point.y);

            this.transform.position = new Vector3(this.transform.position.x, higherHitPoint + minFloatHeight, this.transform.position.z);

            if (this.rigidbody2D.velocity.y < 0)
            {
                this.rigidbody2D.velocity = new Vector2(this.rigidbody2D.velocity.x, 0);
            }
        }
    }
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    private void CallListenersEnter()
    {
        foreach (GroundListener current in groundListener)
        {
            current.playerGroundEnter();
        }
    }

    private void CallListenersExit()
    {
        lastGroundPos = this.transform.position;

        if (this.rigidbody2D.velocity.x < 0)
        {
            lastGroundPos += new Vector3( 0.5f, 0, 0);
        }
        else
        {
            lastGroundPos += new Vector3(-0.5f, 0, 0);
        }

        foreach (GroundListener current in groundListener)
        {
            current.playerGroundExit();
        }
    }
}
