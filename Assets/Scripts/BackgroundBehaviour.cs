﻿using UnityEngine;
using System.Collections;

public class BackgroundBehaviour : MonoBehaviour 
{
    public float minX;
    public float maxX;

    public float mapXMin;
    public float mapXMax;

    public float minY;
    public float maxY;

    public float mapYMin;
    public float mapYMax;

	// Use this for initialization
	void Start () 
    {

	}
	
	// Update is called once per frame
	void Update () 
    {
        float playerFraction = (Player.Instance.transform.position.x - mapXMin) / (mapXMax - mapXMin);

        float backgroundX = Mathf.Lerp(maxX, minX, playerFraction);


        playerFraction = (Player.Instance.transform.position.y - mapYMin) / (mapYMax - mapYMin);

        float backgroundY = Mathf.Lerp(maxY, minY, playerFraction);

        this.transform.localPosition = new Vector3(backgroundX, backgroundY, this.transform.localPosition.z);
	}
}
