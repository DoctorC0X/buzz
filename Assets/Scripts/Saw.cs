﻿using UnityEngine;
using System.Collections;

public class Saw : MonoBehaviour 
{
    public float currentSpeed;

    public float fastSpeed;
    public float normalSpeed;
    public float slowSpeed;

    public Sprite normalSaw;
    public Sprite bloodSaw;

    public SpriteRenderer sawRenderer;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        this.transform.Rotate(new Vector3(0, 0, currentSpeed));
	}
}
