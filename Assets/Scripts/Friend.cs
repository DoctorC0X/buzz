﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Friend : MonoBehaviour 
{
    public FloatBehaviour floater;

    public GameObject spriteRendererGO;
    public Animator anim;

    public Collider2D triggerForDiagonals;

    public bool flee;
    public bool inPrivateTime;

    public bool wall;

    public float direction;
    public float horizontalAcc;

    public float stopWalkAnimBeneath;

    public List<AudioClip> voiceLines;
    public AudioClip myLine;
    public AudioSource voiceSource;

	// Use this for initialization
	void Start () 
    {
        floater = this.GetComponent<FloatBehaviour>();
        anim = spriteRendererGO.GetComponent<Animator>();
        triggerForDiagonals.enabled = false;

        int voiceLineIndex = Random.Range(0, voiceLines.Count);
        myLine = voiceLines[voiceLineIndex];

        voiceSource = GameObject.Find("ScreamBox").GetComponent<AudioSource>();
	}

    void FixedUpdate()
    {
        if (floater.leftHeight != floater.rightHeight)
        {
            // calc angle for diagonal collider
            triggerForDiagonals.enabled = true;

            float opposite = Mathf.Abs(floater.leftHeight - floater.rightHeight);
            float adjacent = floater.colliderOffsetRight.x - floater.colliderOffsetLeft.x;

            float angle = Mathf.Atan(opposite / adjacent) * Mathf.Rad2Deg;

            triggerForDiagonals.gameObject.transform.localEulerAngles = new Vector3(0, 0, angle);
        }
        else
        {
            triggerForDiagonals.enabled = false;
        }

        if (flee && !inPrivateTime && !wall)
        {
            if (direction == 1 && floater.rightHeight < floater.minFloatHeight * 3 ||
                direction == -1 && floater.leftHeight < floater.minFloatHeight * 3)
            {
                anim.Play("Walk");

                this.rigidbody2D.AddForce(new Vector2(horizontalAcc * direction * this.rigidbody2D.mass, 0));
            }
            else
            {
                flee = false;
                this.rigidbody2D.velocity = Vector2.zero;
            }
        }

        if (Mathf.Abs(this.rigidbody2D.velocity.x) < stopWalkAnimBeneath)
        {
            anim.Play("Idle");
        }
    }

	// Update is called once per frame
	void Update () 
    {
	
	}

    public void StartPrivateTime()
    {
        inPrivateTime = true;

        voiceSource.clip = myLine;
        voiceSource.Play();
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.tag.Equals(Tags.player))
        {
            direction = c.transform.position.x < this.transform.position.x ? 1 : -1;

            spriteRendererGO.transform.localScale = new Vector3(direction, 1, 1);

            Player.Instance.friendInDirection = direction * -1;
        }
    }

    void OnTriggerStay2D(Collider2D c)
    {
        if (c.tag.Equals(Tags.player))
        {
            flee = true;
        }
    }

    void OnTriggerExit2D(Collider2D c)
    {
        if (c.tag.Equals(Tags.player))
        {
            Player.Instance.friendInDirection = 0;

            flee = false;
        }
    }

    void OnCollisionEnter2D(Collision2D c)
    {
        if (c.collider.tag.Equals(Tags.wall))
        {
            wall = true;
        }
    }
}
