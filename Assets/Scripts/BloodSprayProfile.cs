﻿using UnityEngine;
using System.Collections;

public class BloodSprayProfile : MonoBehaviour 
{
    public Vector3 localVelocitySpray;
    public Vector3 rndVelocitySpray;

    public Vector3 localVelocityFountain;
    public Vector3 rndVelocityFountain;

	// Use this for initialization
	void Start () 
    {
        this.GetComponent<ParticleRenderer>().sortingOrder = 1;
	}

    void OnEnable()
    {
        ParticleEmitter pe = this.gameObject.GetComponent<ParticleEmitter>();

        if (Random.value > 0.7f)
        {
            pe.localVelocity = new Vector3(localVelocitySpray.x * Player.Instance.friendInDirection * -1, localVelocitySpray.y, localVelocitySpray.z);
            pe.rndVelocity = rndVelocitySpray;
        }
        else
        {
            pe.localVelocity = localVelocityFountain;
            pe.rndVelocity = rndVelocityFountain;
        }
    }
	
	// Update is called once per frame
	void Update () 
    {
	
	}
}
