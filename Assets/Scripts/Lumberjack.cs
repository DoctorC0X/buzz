﻿using UnityEngine;
using System.Collections;

public class Lumberjack : MonoBehaviour 
{
    public bool fading;

    public float alphaValueChange;

    public SpriteRenderer renderer;
    public AudioSource music;

    public AudioSource whistle;

    public GUIText endText;
    public GameObject endSign;

    public bool timerStarted;
    public float timer;
    public float timerMax;

	// Use this for initialization
	void Start () 
    {
        fading = false;

        renderer = GameObject.Find("Fader").GetComponent<SpriteRenderer>();
        music = GameObject.Find("MusicBox").GetComponent<AudioSource>();

        whistle = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (fading)
        {
            if (renderer.color.a < 0.99f)
            {
                Color c = renderer.color;

                renderer.color = new Color(c.r, c.g, c.b, c.a + alphaValueChange);
            }
            else
            {
                timerStarted = true;
                endSign.SetActive(true);
                fading = false;
            }
        }

        if (timerStarted)
        {
            timer += Time.deltaTime;

            if (timer > timerMax)
            {
                endText.enabled = true;
            }
        }
	}

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.tag.Equals(Tags.player))
        {
            Player.Instance.GameEnd();
            
            renderer.enabled = true;
            fading = true;
        }
    }
}
