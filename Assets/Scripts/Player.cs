﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour, GroundListener
{
    private static Player instance;

    public static Player Instance
    {
        get { return instance; }
    }

    public FloatBehaviour floater;
    public JumpBehaviour jumper;
    public ClimbBehaviour climber;
    public AudioSource sawSource;

    public List<GUIText> friendCountGUI;

    public int friendsMade;
    public int friendsNeeded;
    public int totalFriends;

    public float horizontalAcc;

    public float stopWalkAnimBeneath;

    public float friendInDirection;

    private bool inPrivateTime;
    public float privateTimer;
    public float privateTimerMax;
    public GameObject privateTimeGO;
    public GameObject friend;

    public GameObject mapGO;

    public GameObject spriteRendererGO;

    public Animator animFull;
    public Animator animHalf;

    public Saw attachedSaw;

    public SpriteRenderer srFull;
    public SpriteRenderer srHalf;

    public Sprite jumpSpriteFull;
    public Sprite jumpSpriteHalf;

    public float gravityMultiplierFalling;
    public float gravityMultiplierJumping;

    public bool controlsEnabled;

    public float friendPowerBuff;

    public float timeFalling;
    public float timeFallingMax;
    public bool _falling;

    public bool gonnaDie;

    void Awake()
    {
        if (instance != null && instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }
        else
        {
            instance = this;
        }

        friendInDirection = 0;
        controlsEnabled = true;

        floater = this.gameObject.GetComponent<FloatBehaviour>();
        jumper = this.gameObject.GetComponent<JumpBehaviour>();
        climber = this.gameObject.GetComponent<ClimbBehaviour>();
    }

	// Use this for initialization
	void Start () 
    {
        friendsMade = 0;
        floater.groundListener.Add(this);

        Friend[] allFriends = GameObject.FindObjectsOfType<Friend>();

        foreach (Friend current in allFriends)
        {
            if (current.gameObject.tag.Equals(Tags.friend))
            {
                totalFriends++;
            }
        }

        friendCountGUI[0].enabled = false;
        friendCountGUI[0].text = "You got " + friendsMade + " of " + totalFriends + " friends";

        friendCountGUI[1].enabled = false;
        friendCountGUI[1].text = "You need " + ((friendsNeeded - friendsMade) < 0 ? 0 : (friendsNeeded - friendsMade)) + " more for the Power of Friendship *";

        friendCountGUI[2].enabled = false;
        friendCountGUI[2].text = "* Power of Friendship enables you to climb a big tree!";
	}

    void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis(Buttons.horizontal);

        Debug.DrawRay(this.transform.position, this.rigidbody2D.velocity, Color.blue);

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        if (horizontalInput != 0 && controlsEnabled)
        {
            if (horizontalInput < 0 && !climber.leftTouchingWall)
            {
                spriteRendererGO.transform.localScale = new Vector3(-1, spriteRendererGO.transform.localScale.y, spriteRendererGO.transform.localScale.z);
                this.rigidbody2D.AddForce(new Vector2(horizontalAcc * horizontalInput * this.rigidbody2D.mass, 0));
            }
            else if (horizontalInput > 0 && !climber.rightTouchingWall)
            {
                spriteRendererGO.transform.localScale = new Vector3(1, spriteRendererGO.transform.localScale.y, spriteRendererGO.transform.localScale.z);
                this.rigidbody2D.AddForce(new Vector2(horizontalAcc * horizontalInput * this.rigidbody2D.mass, 0));
            }

            animFull.Play("Walk");
            animHalf.Play("Walk");
        }

        if (this.rigidbody2D.velocity.y < 0)
        {
            falling = true;
        }
        else
        {
            falling = false;
        }

        if (Mathf.Abs(this.rigidbody2D.velocity.x) < stopWalkAnimBeneath)
        {
            animFull.Play("Idle");
            animHalf.Play("Idle");
        }

        if (inPrivateTime)
        {
            privateTimer += Time.fixedDeltaTime;

            if (privateTimer > privateTimerMax)
            {
                privateTimer = 0;
                inPrivateTime = false;
                controlsEnabled = true;

                friendsMade++;

                friendCountGUI[0].text = "You got " + friendsMade + " of " + totalFriends + " friends";

                string friendsLeft = "";

                if ((friendsNeeded - friendsMade) > 0)
                {
                    friendsLeft = (friendsNeeded - friendsMade).ToString();
                }
                else
                {
                    friendsLeft = "no";
                }

                friendCountGUI[1].text = "You need " + friendsLeft + " more for the Power of Friendship*";

                attachedSaw.sawRenderer.sprite = attachedSaw.bloodSaw;

                climber.climbAccDecayFactor_specialCurrent = Mathf.Lerp(climber.climbAccDecayFactor, climber.climbAccDecayFactor_specialMax, (float)friendsMade / (float)friendsNeeded);

                Destroy(friend);

                privateTimeGO.SetActive(false);
            }
        }

        if (falling)
        {
            if (climber.leftTouchingWall || climber.rightTouchingWall)
            {
                falling = false;
            }

            timeFalling += Time.fixedDeltaTime;

            if (!gonnaDie && timeFalling > timeFallingMax)
            {
                gonnaDie = true;
            }
        }

        if (Input.GetButtonDown(Buttons.map) && controlsEnabled)
        {
            mapGO.SetActive(!mapGO.activeSelf);
            
            foreach (GUIText current in friendCountGUI)
            {
                current.enabled = !current.enabled;
            }
        }
    }
	
	// Update is called once per frame
	void Update () 
    {
        
	}

    public void GameEnd()
    {
        horizontalAcc = 25;
        //controlsEnabled = false;
    }

    // dying and respawning when falling to high
    public bool falling
    {
        get { return _falling; }
        set
        {
            if (_falling != value)
            {
                if (value)
                {
                    Player.Instance.floater.gravityMultiplier = gravityMultiplierFalling;
                }
                else
                {
                    Player.Instance.floater.gravityMultiplier = gravityMultiplierJumping;
                    timeFalling = 0;
                    gonnaDie = false;
                }
                _falling = value;
            }
        }
    }

    public void Respawn()
    {
        gonnaDie = false;
        falling = false;
        timeFalling = 0;
    }

    void OnCollisionEnter2D(Collision2D c)
    {
        if (c.gameObject.tag.Equals(Tags.friend))
        {
            inPrivateTime = true;
            controlsEnabled = false;
            mapGO.SetActive(false);

            foreach (GUIText current in friendCountGUI)
            {
                current.enabled = false;
            }

            friend = c.gameObject;
            friend.GetComponent<Friend>().StartPrivateTime();

            sawSource.Play();

            privateTimeGO.SetActive(true);
        }
    }

    void OnCollisionExit2D(Collision2D c)
    {
        
    }

    public void playerGroundEnter()
    {
        falling = false;

        animFull.enabled = true;
        animHalf.enabled = true;
    }

    public void playerGroundExit()
    {
        animFull.enabled = false;
        animHalf.enabled = false;

        srFull.sprite = jumpSpriteFull;
        srHalf.sprite = jumpSpriteHalf;
    }
}
