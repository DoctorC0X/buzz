﻿using UnityEngine;
using System.Collections;

public class Deer : MonoBehaviour 
{
    public FloatBehaviour floater;
    public Friend friendBehaviour;

    public Vector2 jumpAcc;
    public Vector2 startScale;
    public Vector2 targetScale;

    public float scalingEndTime;
    public float scalingDuration;

    public Collider2D collider;

    public Animator anim;
    public SpriteRenderer deerRenderer;
    public Sprite jumpSprite;

    public bool jumped;

    public float lifeTimeAfterJump;

	// Use this for initialization
	void Start () 
    {
        floater = this.GetComponent<FloatBehaviour>();
        friendBehaviour = this.GetComponent<Friend>();
	}

    void FixedUpdate()
    {
        
    }
	
	// Update is called once per frame
	void Update () 
    {
        if (jumped)
        {
            if (this.rigidbody2D.velocity.y < 0)
            {
                this.rigidbody2D.gravityScale = 4f;
            }

            this.transform.localScale = Vector2.Lerp(startScale, targetScale, Time.time / scalingEndTime);

            lifeTimeAfterJump -= Time.deltaTime;

            if (lifeTimeAfterJump < 0)
            {
                Destroy(this.gameObject);
            }
        }
	}

    void OnCollisionEnter2D(Collision2D c)
    {
        if (c.gameObject.tag.Equals(Tags.player))
        {
            collider.enabled = false;
            floater.enabled = false;
            this.rigidbody2D.gravityScale = 1;

            scalingEndTime = Time.time + scalingDuration;
            jumped = true;

            startScale = this.transform.localScale;

            anim.enabled = false;
            deerRenderer.sprite = jumpSprite;

            this.transform.position += new Vector3(0, 0, -1);
            this.rigidbody2D.AddForce(this.rigidbody2D.mass * new Vector2(jumpAcc.x * friendBehaviour.direction, jumpAcc.y), ForceMode2D.Impulse);

            friendBehaviour.enabled = false;
        }
    }
}
