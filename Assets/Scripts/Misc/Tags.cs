﻿using UnityEngine;
using System.Collections;

public class Tags
{
    public const string player = "Player";
    public const string ground = "Ground";
    public const string wall = "Wall";
    public const string wall_special = "Wall_Special";
    public const string friend = "Friend";
}
