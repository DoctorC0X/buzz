﻿using UnityEngine;
using System.Collections;

public class JumpBehaviour : MonoBehaviour, GroundListener
{
    public float jumpForceInital;
    public float jumpForcePerUpdate;
    public bool jumpStarted;

    public float jumpTimer;
    public float jumpTimerMax;

    public float coveredDistance;
    public Vector3 lastFramePos;

    public bool midAir;

	// Use this for initialization
	void Start () 
    {
        Player.Instance.floater.groundListener.Add(this);
	}

    void FixedUpdate()
    {
        if (!Player.Instance.climber.climbing)
        {
            if (Input.GetButton(Buttons.jump) && jumpStarted)
            {
                this.rigidbody2D.AddForce(new Vector2(0, jumpForcePerUpdate * this.rigidbody2D.mass), ForceMode2D.Force);
            }

            if (Input.GetButton(Buttons.jump) && !midAir && !jumpStarted && Player.Instance.controlsEnabled)
            {
                jumpStarted = true;
                jumpTimer = 0;

                coveredDistance = 0;
                lastFramePos = this.transform.position;

                this.rigidbody2D.AddForce(new Vector2(0, jumpForceInital * this.rigidbody2D.mass), ForceMode2D.Impulse);
            }

            if (jumpStarted)
            {
                jumpTimer += Time.fixedDeltaTime;

                if (jumpTimer > jumpTimerMax)
                {
                    jumpStarted = false;
                    jumpTimer = 0;
                }
            }

            if (midAir)
            {
                coveredDistance += Mathf.Abs(this.transform.position.x - lastFramePos.x);

                lastFramePos = this.transform.position;
            }
            
            if (midAir)
            {
                coveredDistance += Mathf.Abs(this.transform.position.x - lastFramePos.x);

                lastFramePos = this.transform.position;
            }
        }
    }

	// Update is called once per frame
	void Update () 
    {
	
	}

    public void playerGroundEnter()
    {
        midAir = false;
    }

    public void playerGroundExit()
    {
        midAir = true;
    }
}
