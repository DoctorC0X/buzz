﻿using UnityEngine;
using System.Collections;

public interface GroundListener
{
    void playerGroundEnter();
    void playerGroundExit();
}
