﻿using UnityEngine;
using System.Collections;

public class TriggerSound : MonoBehaviour {

    public AudioSource whistle;
    public AudioSource music;

	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.tag.Equals(Tags.player))
        {

            whistle.time = music.time;
            whistle.Play();
        }
        
    }
}
